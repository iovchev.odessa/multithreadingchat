package org.hillel.chat.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerMain {
    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(8081)) {
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("Got new connection.");
                ServerClientThread client = new ServerClientThread(socket);
                Thread clientThread = new Thread(client);
                clientThread.setDaemon(true);
                clientThread.start();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
