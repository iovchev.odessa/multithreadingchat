package org.hillel.chat.client;


import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Client {
    private BufferedWriter writer;
    private BufferedReader reader;
    private BufferedReader input;
    private Socket socket;
    private static final SimpleDateFormat dt1 = new SimpleDateFormat("HH:mm:ss");

    public Client() {
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress("localhost", 8081));
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
            DisplayMessage displayMessage = new DisplayMessage();
            displayMessage.setDaemon(true);
            displayMessage.start();
            input = new BufferedReader(new InputStreamReader(System.in));
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            new WriteMessage().start();
        } catch (Exception e) {
        }
    }

    private class WriteMessage extends Thread {
        @Override
        public void run() {
            try {
                while (true) {
                    String time = dt1.format(new Date());
                    String consoleValue = input.readLine();
                    if (consoleValue == null || consoleValue.isEmpty()) {
                        continue;
                    }
                    writer.write(consoleValue + "<" + time + ">\n");
                    if ("exit".equals(consoleValue) || socket.isClosed()) {
                        writer.close();
                        reader.close();
                        break;
                    }
                    writer.flush();
                }
            } catch (IOException ex) {
            }
        }
    }

    private class DisplayMessage extends Thread {
        @Override
        public void run() {
            while (true) {
                try {
                    String message = reader.readLine();
                    if (message == null || message.isEmpty()) {
                        Thread.sleep(1000);
                    }
                    System.out.println(message);
                } catch (Exception ex) {
                }
            }
        }
    }
}

